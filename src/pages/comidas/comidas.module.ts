import { NgModule } from '@angular/core';
import { IonicPageModule, NavController, NavParams } from 'ionic-angular';
import { ComidasPage } from './comidas';

@NgModule({
  declarations: [
    ComidasPage,
  ],
  imports: [
    IonicPageModule.forChild(ComidasPage),
  ],
})
export class ComidasPageModule {
  public appName : String = "Olá mundo";
  
  constructor(public navCtrl: NavController, public navParams: NavParams){

  }
    ionViewDidLoad(){
      console.log('ionViewDidLoad noticiasPage');
      alert('mensagem qualquer');
    }
}
