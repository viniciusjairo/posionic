import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProjetoProvider } from '../../providers/api-projeto/api-projeto';

/**
 * Generated class for the ComidasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-comidas',
  templateUrl: 'comidas.html',

  providers: [
    ApiProjetoProvider
  ]
})
export class ComidasPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private FoodProvider: ApiProjetoProvider) {
  }
  public listaPersonagens = new Array<any>();

  ionViewDidLoad() {
    this.FoodProvider.getLatestMovies().subscribe(
      data => {
        const response = (data as any);
        const obj = JSON.parse(response._body);
        console.log(obj);
        this.listaPersonagens = obj.results;
      },
      error => {
        console.log(error);
      }
    )
  }

}
