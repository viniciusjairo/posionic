import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class ApiProjetoProvider {

  public api_url = "https://swapi.co/api/";


  constructor(public http: Http) {
    console.log('Hello ApiProjetoProvider Provider');
  }

  getLatestMovies() {
    return this.http.get(this.api_url + "people");
  }

}
